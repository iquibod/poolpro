import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { HttpModule }      from '@angular/http';

import { AppComponent } from './app.component';
import { DealersComponent } from './dealers/dealers.component';
import { DealerDetailComponent } from './dealer-detail/dealer-detail.component';

import { DealerService }          from './dealer.service';


@NgModule({
  declarations: [
    AppComponent,
    DealersComponent,
    DealerDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
  ],
  providers: [ DealerService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
