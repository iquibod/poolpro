import { Component, OnInit } from '@angular/core';

import { DealerService } from '../dealer.service'

@Component({
  selector: 'app-dealers',
  templateUrl: './dealers.component.html',
  styleUrls: ['./dealers.component.css']
})
export class DealersComponent implements OnInit {

	dealers = [];

	constructor(
		private dealerService: DealerService,
	) { }

	ngOnInit() {
		let r = this.dealerService.getDealers()
			.subscribe(dealers => this.dealers = dealers);

		console.log(r);
	}

}
