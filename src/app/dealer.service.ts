import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class DealerService {

	private _url: string = '../assets/data/dealers.json';

	constructor(
		private _http: Http,
	) { }


	getDealers = (): Observable<Response[]> => {
	    return this._http.get( this._url )
            .map((response) => response.json() );
    }

}
